import React from 'react';
import ItemForm from '../item-form/item-form'
import ItemsList from '../items-list/items-list'
import {v4} from 'uuid'

const CustomTitle = () => ( <h1> ToDo List</h1> )

class TodoApp extends React.Component {
  constructor(props){
    super(props)

    this.state ={
      listOfItems: [],
      inputText: ""

    }
  }

  handleInputTextChange = (e) => {
    this.setState({inputText: e.currentTarget.value})
  }

  handleItemFormSubmit = (e) => {
    e.preventDefault();

    const newItemInList = {name: e.currentTarget.todo.value, id: v4() }

    this.setState({
      listOfItems: [...this.state.listOfItems, newItemInList], inputText: ""
    })
  }

  handleTaskDone = (e) => {
    const itemId = e.target.dataset.id
    const newListOfItems = this.state.listOfItems.filter((item) =>  item.id !== itemId )
    this.setState({listOfItems: newListOfItems})
  }

  render(){

    return(
      <div>

        <CustomTitle />

        <ItemForm
          handleItemFormSubmit={this.handleItemFormSubmit}
          handleInputTextChange={this.handleInputTextChange}
          inputText={this.state.inputText}
        />

        <ItemsList
          listOfItems={this.state.listOfItems}
          handleTaskDone={this.handleTaskDone}
        />

      </div>
    )
  }

}

export default TodoApp
