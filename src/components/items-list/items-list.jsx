import React from 'react'

function Item(props){
  return(
    <li>
      <h4> {props.name} </h4>
      <a
        data-id={props.id}
        onClick={props.handleTaskDone} >Done
      </a>
    </li>
  )
}

class ItemsList extends React.Component {

  render(){
    const ListItems = this.props.listOfItems.map( (item) =>
      <Item
        key={item.id}
        name={item.name}
        id={item.id}
        handleTaskDone={this.props.handleTaskDone}
      />
    );

    return(
      <ul>
        {ListItems}
      </ul>
    )
  }
}

export default ItemsList
