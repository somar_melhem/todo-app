import React from 'react'

class ItemForm extends React.Component {
  constructor(props){
    super(props)
    this.state ={
      text: ""
    }

  }

  render(){
    return(
      <form onSubmit={this.props.handleItemFormSubmit} >
        <label>
          <input
            type="text"
            name="todo"
            value={this.props.inputText}
            onChange={this.props.handleInputTextChange}
          />
        </label>

        <input
          type="submit"
          value="Add"
        />
      </form>
    )
  }
}

export default ItemForm
